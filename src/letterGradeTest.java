import static org.junit.Assert.*;

import org.junit.Test;

public class letterGradeTest {

	@Test
	public void test5_a() {
		letterGrade lg = new letterGrade();
		assertEquals('X', lg.letterGrade(-10));
		assertEquals('F', lg.letterGrade(30));
		assertEquals('D', lg.letterGrade(65));
		assertEquals('C', lg.letterGrade(75));
		assertEquals('B', lg.letterGrade(85));
		assertEquals('A', lg.letterGrade(95));
		assertEquals('X', lg.letterGrade(110));
	}
	
	@Test
	public void test5_b() {
		letterGrade lg = new letterGrade();
		assertEquals('X', lg.letterGrade(-1));
		assertEquals('F', lg.letterGrade(0));
		assertEquals('F', lg.letterGrade(59));
		assertEquals('D', lg.letterGrade(60));
		assertEquals('D', lg.letterGrade(69));
		assertEquals('C', lg.letterGrade(70));
		assertEquals('C', lg.letterGrade(79));
		assertEquals('B', lg.letterGrade(80));
		assertEquals('B', lg.letterGrade(89));
		assertEquals('A', lg.letterGrade(90));
		assertEquals('A', lg.letterGrade(100));
		assertEquals('X', lg.letterGrade(101));
	}

	
	
}
